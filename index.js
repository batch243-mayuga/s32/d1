// Imported http through require directive
let http = require("http");

// Define the port number that the server will be listening to
const port = 4000;

// We used the createServer method
http.createServer(function(request,response){
	// create a condition and a response when the route "/items" is accessed

	// HTTP Routing Methods: Get, Post, Put, Delete
	if(request.url == "/items" && request.method == "GET"){

		// HTTP method of the incoming request can be accessed via the method property of the request parameter
		// The method "GET" means that we will be retrieving or reading an information
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Data retrieve from the database')

	}
		else if(request.url == "/items" && request.method == "POST"){

			// Request the '/items' path and "SENDS" information
			response.writeHead(200, {'Content-Type' : 'text/plain'});
			response.end('Data to be sent to the database')
		}
		
		// use listen method for the server to run in a specified port
}).listen(4000)

// console log to monitor the server
console.log('Server running at localhost: 4000');